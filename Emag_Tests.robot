***Settings***
Documentation  Verval proj. 2, web tests are for the emag.ro site
Library  Selenium2Library
Resource  keywords.robot
Test Setup  Open Emag
Test Teardown  Close Browser


***Variables***

${lenovo}  lenovo legion
${samsung}  samsung tv
${hungarian_word}  léghajó
${random_word}  asdniafbiausbf
${help_text}  telefoane
${emag}  https://www.emag.ro/
${favourites}  https://www.emag.ro/favorites?ref=ua_favorites

***Test Cases***

Search for invalid products
    [Template]  Search invalid product
    ${hungarian_word}
    ${random_word}
    
Look up Lenovo Legion
    Search for product  ${lenovo}
    Verify if search has results

Add Lenovo Legion to cart
    Search for product  ${lenovo}
    Add item to cart
    Verify if item was added to cart

Add items to favourites
    Given User adds product to favourites  ${samsung}
    When he goes to the favourites
    Then the favourites shouldnt be empty

Use Emag Help search
    Click Link  class:navbar-aux-help-link
    Input Text  class:info-home-search-input  ${help_text}
    Page Should Not Contain  Cautarea nu a returnat nici un rezultat

Delete from favourites
    Given User adds product to favourites  ${samsung}
    When he goes to the favourites
    Then the favourites shouldnt be empty
    Click Element  class:em-delete
    Wait Until Element Is Visible  class:empty-list-img
    Page Should Contain  Hmm, niciun produs in lista ta.

Delete from cart
    Search for product  ${samsung}
    Add item to cart
    Verify if item was added to cart
    Go To  https://www.emag.ro/cart/products?ref=cart
    Click Element  //*[@id="vendorsContainer"]/div/div[1]/div[1]/div[2]/div[1]/div[3]/a[1]
    Sleep  2s
    Page Should Contain  Cosul tau este gol