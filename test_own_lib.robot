***Settings***
Documentation  Verval proj. 2, web tests are for the emag.ro site
Library  Selenium2Library
Library     Util.py
Resource  keywords.robot
Test Setup  Open Emag
Test Teardown  Close Browser

***Variables***

${lenovo}  lenovo legion
${samsung}  samsung tv
${emag}  https://www.emag.ro/
${favourites}  https://www.emag.ro/favorites?ref=ua_favorites

***Test Cases***

Validate if Lenovo Legion is laptop
    Search for product  ${lenovo}
    Go to product page
    ${name}  Execute Javascript  return document.evaluate('//*[@id="main-container"]/section[1]/div/div[1]/h1', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent;
    ${result}  is Laptop  ${name}
    Should Be True  ${result}

Verify that Samsung tv is expensive
    Search for product  ${samsung}
    Go to product page
    ${price}  Execute Javascript  return parseInt(document.evaluate('//*[@id="main-container"]/section[1]/div/div[2]/div[2]/div/div/div[2]/form/div[1]/div[1]/div/div/p', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent.replace(/\s/g, '').split('L')[0].replace('.','')) / 100
    ${result}  is Expensive  ${price}
    Should Be True  ${result}

Verify that Samsung tv and Lenovo Legion don't have the same price
    Search for product  ${samsung}
    Go to product page
    ${price}  Execute Javascript  return parseInt(document.evaluate('//*[@id="main-container"]/section[1]/div/div[2]/div[2]/div/div/div[2]/form/div[1]/div[1]/div/div/p', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent.replace(/\s/g, '').split('L')[0].replace('.','')) / 100
    Search for product  ${lenovo}
    Go to product page
    ${price2}  Execute Javascript  return parseInt(document.evaluate('//*[@id="main-container"]/section[1]/div/div[2]/div[2]/div/div/div[2]/form/div[1]/div[1]/div/div/p', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent.replace(/\s/g, '').split('L')[0].replace('.','')) / 100
    ${result}  Are Equal  ${price}  ${price2}
    Should Not Be True  ${result}