***Keywords***

Open Emag
    [Documentation]  Opens emag.ro
    Open Browser  ${emag}

Search invalid product
    [Arguments]  ${product}
    Click Element  id:searchboxTrigger
    Input Text  id:searchboxTrigger  ${product}
    Click Button    class:searchbox-submit-button
    Page Should Contain     0 rezultate pentru:

Search for product
    [Arguments]  ${product}
    Input Text  id:searchboxTrigger  ${product}
    Click Button    class:searchbox-submit-button

Verify if search has results
    Page Should Contain Element  id:card_grid

Add item to cart
    Click Button  //*[@id="card_grid"]/div[1]/div/div/div[3]/div[3]/form/button

Verify if item was added to cart
    Wait Until Element Is Visible  class:modal-dialog
    Page Should Contain  Produsul a fost adaugat in cos

Close added to cart popup
    Click Button  /html/body/div[14]/div/div/div[1]/button

User adds product to favourites
    [Arguments]  ${product}
    Search for product  ${product}
    Verify if search has results
    Click Button  //*[@id="card_grid"]/div[1]/div/div/div[1]/div[2]/button[2]

He goes to the favourites
    Go To  ${favourites}

The favourites shouldnt be empty
    Page Should Contain Element  id:list-of-favorites

Go to product page
    Click Link  //*[@id="card_grid"]/div[1]/div/div/div[1]/div[1]/a